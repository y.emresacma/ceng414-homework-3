import sys
import pickle
import csv
from datetime import datetime
import pprint
import numpy as np
import math

from hw4_movie import Movie

reload(sys)
sys.setdefaultencoding('utf8')

PERSON_NAMES = []
IGNORED_WORDS = []

def load_data_from_pickle_file(file_name):
    try:
        with open(file_name, 'rb') as input:
            data = pickle.load(input)

        return data
    except:
        return None


def save_data_to_pickle_file(file_name, data):
    try:
        with open(file_name, 'wb') as output:
            pickle.dump(data, output, pickle.HIGHEST_PROTOCOL)

        return 1
    except:
        return 0


def get_person_names():
    person_names = list()
    file = open("person_names.txt", 'r') # Open person names txt file
    for line in file:   # add names to PERSON_person_namesNAMES list
        person_names.append(line[:-1])
    person_names[-1] = line
    return person_names


def get_ignored_words():
    ignored_words = list()
    file = open("ignored_words.txt", "r") # Open ignored word txt file
    for line in file:   # add words to IGNORED_WORDS list
        ignored_words.append(line[:-1])
    ignored_words[-1] = line
    return ignored_words


def read_movies_and_genres(movie_csv_filename):
    MOVIES = list()
    allGenres = list()

    file = open(movie_csv_filename, "r")
    for line in file: # ignore first line
        break
    for line in file: # add Movie object to MOVIES list
        line.replace("   ", " ") # replace long spaces with one single space
        line.replace("  ", " ")
        if line[0] == " ": # if line starts with space, delete that space
            line = line[1:]
        if line[-1] == " ": # if line ends with space, delete it
            line = line[:-1]                

        features = line.split(">") # Extract features
        title = features[0] 
        releaseDate = datetime.strptime(features[1], '%Y-%m-%d')

        overview = features[2].lower()
        language = features[3]
        genres = features[4].split(",")
        genres[-1] = genres[-1][:-1] # take out '/n' character which at the end
        for i in range(len(genres)):
            if genres[i] not in allGenres: # to make it distinct
                allGenres.append(genres[i])
        
        MOVIES.append(Movie(title, releaseDate, overview, language, genres)) # Make movie object from extracted features and append them list

    lenGenres = len(allGenres)
    for i in range(lenGenres):  # make string in ascending order
        minGenre = allGenres[i]
        minIndex = i
        j = i 
        while j < lenGenres:
            if allGenres[j] < minGenre:
                minGenre = allGenres[j]
                minIndex = j
            j += 1
        swap = allGenres[i]
        allGenres[i] = minGenre
        allGenres[minIndex] = swap

    return MOVIES, allGenres    

"""
    Tokenize movie overviews (split by space), save tokenized overviews as list of lists.
    Return tokenized_overviews list.
    
    Example:
        Document_1: "uzaylilar tarafindan kacirildim"
        Document_2: "evet tarafindan"
        
        Return value will be:
            [ ["uzaylilar", "tarafindan", "kacirildim"], ["evet", "tarafindan"] ]
"""

def get_tokenized_overviews(movies):
    tokenizedOverviews = list()

    for i in range(len(movies)):
        overviewWords = movies[i].overview.split(" ")

        if overviewWords[-1] == "":
            overviewWords = overviewWords[:-1]
        if overviewWords[-1] == " ":
            overviewWords = overviewWords[:-1]
        if overviewWords[-1] == "  ":
            overviewWords = overviewWords[:-1]
        tokenizedOverviews.append(overviewWords)
    
    return tokenizedOverviews


def find_word_set(tokenized_overviews):
    wordSet = list()
    for i in range(len(tokenized_overviews)): # add distinct words to list
        for j in range(len(tokenized_overviews[i])):
            if tokenized_overviews[i][j] not in wordSet:
                wordSet.append(tokenized_overviews[i][j])
    
    return sorted(wordSet)

"""
    Find word histograms from tokenized overviews and word set.
    Return word histograms for each overview, as list of histograms. Histogram key order is not important.
    In below example, word set is not sorted. In your case, it will already be sorted in previous steps.

    Example:
        Tokenized_overviews:    [ ["bana", "bir", "sopa", "bir", "de", "bez", "bulun", "abi"], ["uzaylilar", "tarafindan", "kacirildim", "evet", "tarafindan"] ]
        Word set:               Distinct set of above.

        Return value will be:
            [
                { "uzaylilar": 0, "tarafindan": 0, "evet": 0, "kacirildim": 0, "bana": 1, "bir": 2, "de": 1, "sopa": 1, "bez": 1, "bulun": 1, "abi": 1 },
                { "uzaylilar": 1, "tarafindan": 2, "evet": 1, "kacirildim": 1, "bana": 0, "bir": 0, "de": 0, "sopa": 0, "bez": 0, "bulun": 0, "abi": 0 }
            ]
"""

def find_word_histograms(tokenized_overviews, word_set):
    histogram = []
    for i in range(len(tokenized_overviews)):
        histogram.append({})
        for j in range(len(word_set)):
            histogram[i][word_set[j]] = 0
    
    for i in range(len(tokenized_overviews)):
        for j in range(len(tokenized_overviews[i])):
            histogram[i][tokenized_overviews[i][j]] += 1
 
    return histogram             

"""
    Calculate Inverse Document Frequency (IDF) of ALL words in word set.
    You should use tokenized overviews. Return word-idf value dictionary.
    
    IDF formula (use natural logarithm in below formula):
    
        IDF(word) = LOG( Number of all movies / Number of movie overviews containing word )
        
    Return value:
        {
            "word_1": value_1,
            "word_2": value_2,
            "word_3": value_3,
            ...
        }
"""

def calculate_idf_scores_of_words(tokenized_overviews, word_set):
    lenMovies = len(tokenized_overviews)
    result = {}
    for i in range(len(word_set)):
        result[word_set[i]] = 0.0
    
    for i in range(len(word_set)):
        for j in range(len(tokenized_overviews)):
            if word_set[i] in tokenized_overviews[j]:
                result[word_set[i]] += 1.0
        result[word_set[i]] = math.log(float(lenMovies) / result[word_set[i]])
    
    
    return result

"""
    Calculate TF-IDF scores for each movie overview.
    Return value dimensions will be <number of movies> x <number of words in word set>
    Return TF-IDF scores as Numpy array.
    
    Formulas:
        TF(word) = Number of occurences of word in the overview / Number of occurences of word with maximum number of occurences in same overview.
        
        Example:
            Word set:           ( "word_1", "word_2", "word_3", "word_4", "word_5" )
            Overview histogram: { "word_1": 3, "word_2": 2, "word_3":0, "word_4": 1, "word_5": 0 }
            TF("word_1") = 3/3 = 1.0
            TF("word_2") = 2/3 = 0.66
            TF("word_3") = 0/3 = 0
            TF("word_4") = 1/3 = 0.33
            TF("word_5") = 0/3 = 0
    
        TF_IDF(word) = TF(word) * IDF(word)
    
    Return value:
        [
            [tf_idf_word_1_overview_1, tf_idf_word_2_overview_1, tf_idf_word_3_overview_1, tf_idf_word_4_overview_1, tf_idf_word_5_overview_1 ],
            [tf_idf_word_1_overview_2, tf_idf_word_2_overview_2, tf_idf_word_3_overview_2, tf_idf_word_4_overview_2, tf_idf_word_5_overview_2 ],
            [tf_idf_word_1_overview_3, tf_idf_word_2_overview_3, tf_idf_word_3_overview_3, tf_idf_word_4_overview_3, tf_idf_word_5_overview_3 ],
            ...
    
        
"""

def calculate_tf_idf_scores_of_words(word_set, word_histograms, idf_scores):
    tfIdfList = []
    for i in range(len(word_histograms)):
        tfIdfList.append([])
        for j in range(len(word_set)):
            tfIdfList[i].append(0.0)
    
    for i in range(len(word_histograms)): 
        maxValue = word_histograms[i][max(word_histograms[i], key=lambda k: word_histograms[i][k])]
        for j in range(len(word_set)):
            tfIdfList[i][j] = (float(word_histograms[i][word_set[j]]) / maxValue) * idf_scores[word_set[j]]
    
    return tfIdfList

"""
    Choose words with best scores as features from each movie overview.
        tf_idf_scores:              List of tf-idf scores.
        word_set:                   Set of ALL DISTINCT words from ALL movie overviews.
        num_of_powerful_features:   An integer threshold for selecting words as features.
    
    Construct a new word set with below steps:
        1- Look at tf-idf scores of a movie overview, select words with top N scores from that movie overview.
           Don't forget to check whether a word is in stop_words.
           Don't forget to discard words such as 1, 1942, 2015. Integer features are not suitable.
           Choose N words AFTER DISCARDING above type of words. Keep looking for scores until you have N suitable
           features.
           
        2- Add words you found in step 1 to the new word set.
        3- Do steps 1 and 2 for all movie overviews.
        
    Return new, reduced word set in the end, in ascending [A-Z] order.
    
        
    Example for single movie overview:
        word_set =                  [word_1, word_2, word_3, word_4, word_5, word_6, word_7]
        tf_idf_scores_of_movie =    [0,      5.7,    0,      5.9,    2.3,    3.0,    4.7]
        num_of_powerful_features =  4
        
        Then these words will be added to returned word set: word_4, word_2, word_7, word_6
            
        *** This is just for giving you an example. Parameters, lengths can change.
    
"""

def feature_selection(tf_idf_scores, word_set, num_of_powerful_features, stop_words):
    features = []

    for i in range(len(tf_idf_scores)):
        j = 0
        orderedScores = sorted(zip(tf_idf_scores[i], word_set), reverse=True)
        index = 0
        while j < num_of_powerful_features:
            if (orderedScores[index][1] not in stop_words) and (not orderedScores[index][1].isdigit()):
                if orderedScores[index][1] not in features:
                    features.append(orderedScores[index][1])
                j += 1
            index += 1
   
    lenFeatures = len(features)
    for i in range(lenFeatures):

        minFeature = features[i]
        minIndex = i
        j = i

        while j < lenFeatures:
            if features[j] < minFeature:
                minFeature = features[j]
                minIndex = j
            j+=1

        swap = features[i]
        features[i] = minFeature
        features[minIndex] = swap
    
    return features


"""
    With tokenized overviews and reduced word set, construct feature matrix.
    Feature matrix shape will be <number of movies> X <number of words in reduced word set>
    Final object will be list of feature lists. Return final object as Numpy array.
    
    For each word in reduced word set
        - If tokenized overview of a movie contains the word, place 1.
        - Place 0 otherwise.
        
    Example for a movie:
        Tokenized words:    [word_1, word_7, word_1, word_6, word_2, word_4, word_5]
        Reduced word set:   (word_1, word_3, word_4, word_7, word_10]
        
        This list should be appended to return value: [1 0 1 1 0]
"""

def construct_feature_matrix(tokenized_overviews, reduced_word_set):
    featureMatrix = []
    for i in range(len(tokenized_overviews)):
        featureMatrix.append([])
    
    for i in range(len(tokenized_overviews)):
        
        for j in range(len(reduced_word_set)):
            if reduced_word_set[j] in tokenized_overviews[i]:
                featureMatrix[i].append(1)
            else:
                featureMatrix[i].append(0)
    
    return featureMatrix

"""
    Construct label list from genres of movies and all genres found in read_movies_and_genres().
    Final object will be list of label lists. Return final object as Numpy array.
    
    
    For each genre in all genres
        - If genre is in movie's genres, place 1
        - Place 0 otherwise.
        
    Example for a movie:
        Movie genres:       Action, Drama, Science-Fiction
        All genres:       Action, Adventure, Drama, Romance, Science-Fiction
        
        This list should be appended to return value: [1 0 1 0 1]    
"""

def construct_labels(movies, all_genres):
    genresMatrix = list()
    for i in range(len(movies)):
        genresMatrix.append([])
    
    for i in range(len(movies)):
        for j in range(len(all_genres)):
            if all_genres[j] in movies[i].genres:
                genresMatrix[i].append(1)
            else:
                genresMatrix[i].append(0)
    
    return genresMatrix


def main():

    global PERSON_NAMES, IGNORED_WORDS

    if len(sys.argv) < 2:
        print "Usage: python hw4.py <movie_csv_filename>"
        sys.exit(1)

    else:

        """
            In real life, you can't use all of the words in a text document, you will be challenged with memory
            restrictions, performance issues, data clean-up etc.

            You will find yourself in a situation that you need to get rid of irrelevant words in a text document.
            This is when you should calculate TF-IDF scores and do feature selection. After feature selection, you can
            proceed and use whatever classifier, clustering algorithm you want. TF-IDF is just one of the measurements
            to order importance of words in a text document. You can see the steps of TF-IDF below.


            STEPS FOR CALCULATING TF-IDF SCORES:
                1-  Tokenize movie overviews (split by space), save tokenized overviews as list of lists. Implement
                    get_tokenized_overviews() function.

                2-  Find all distinct words from all movie overviews, construct word set. Word set is our raw
                    features, we will do feature selection among them. Implement find_word_set() function.

                3-  Have a word histogram for each movie overview. You will use this histogram for Term Frequency (TF)
                    calculation. Implement find_word_histograms() function.

                4-  Calculate Inverse Document Frequency (IDF) of ALL words in word set you constructed in step 2. You should
                    use tokenized format of the overviews you found in step 1. Implement calculate_idf_scores_of_words()
                    function.
                
                5-  Calculate TF-IDF scores. Use word histograms, tokenized overviews, word set you found above.
                    Implement calculate_tf_idf_scores_of_words() function.
                    
                6-  Choose words with best scores as features from each movie overview. Implement feature_selection()
                    function.
                
                7-  With tokenized overviews and reduced word set, construct feature matrix. This feature matrix
                    will be processed by classifier or clustering algorithm. Implement construct_feature_matrix()
                    function.
                    
                8-  Construct label list from genres of movies and all genres found in read_movies_and_genres().
                    Implement construct_labels() function.

        """

        # TODO: Implement here
        # You can change below lines as well, they are just for example usage of pickle functions.
        # You can use pickle data handling functions in order to prevent re-calculation of things in every run, and speed up your work. 

        movie_csv_filename = sys.argv[1]
        data = load_data_from_pickle_file("./pickle_data/all_movies_genres.pkl")

        if data is None:
            movies, all_genres = read_movies_and_genres(movie_csv_filename)

            save_data_to_pickle_file("./pickle_data/all_movies_genres.pkl", (movies, all_genres))

        else:
            movies = data[0]
            all_genres = data[1]


        PERSON_NAMES = get_person_names()

        IGNORED_WORDS = get_ignored_words()

        ALL_STOP_WORDS = IGNORED_WORDS + PERSON_NAMES

        tok_overviews = get_tokenized_overviews(movies)

        word_set = find_word_set(tok_overviews)

        word_histograms = find_word_histograms(tok_overviews, word_set)

        idf_word_map = calculate_idf_scores_of_words(tok_overviews, word_set)

        tf_idf_scores = calculate_tf_idf_scores_of_words(word_set, word_histograms, idf_word_map)

        # select 8 features (remember that this is just an example)
        best_features_count = 8

        reduced_word_set = feature_selection(tf_idf_scores,word_set, best_features_count, ALL_STOP_WORDS)

        feature_matrix = construct_feature_matrix(tok_overviews,reduced_word_set)

        labels = construct_labels(movies, all_genres)

        # Now you are ready to do predictions!

if __name__ == "__main__":
    main()