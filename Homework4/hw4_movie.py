class Movie:
    def __init__(self, title, date, overview, language, genres):
        self.title = title
        self.date = date
        self.overview = overview
        self.language = language
        self.genres = genres

    def __str__(self):
        return "Movie: %s\n\tDate: %s\n\tGenre(s): %s" % (self.title, self.date, ", ".join(self.genres))
