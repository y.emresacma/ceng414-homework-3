**Homework2:** Making some experiment with java by using weka tools

**Homework3:** Implementing K-Means clustering algorithm to do clustering of points in 3D space with python

**Homework4:**  Implementing text mining on movie data. Data is related to the movies released in last 10 years. Main job is predicting genre of movies from movies overview with python.