import sys
import random
from datetime import datetime

import input_reader as reader
import hw3_kmeans_plotter as plotter
from hw3_kmeans import KMeans
from hw3_kmeans import prepare_means

# define global variables
locations_file = None
locations = []


def main():
    global locations_file

    if len(sys.argv) < 2:
        print "Usage: python hw3_main.py <input_file_name>"

    else:
        locations_file = sys.argv[1]

        file_metadata = reader.read_input_file(locations_file)
        locations = file_metadata['locations']
        x_interval = file_metadata['x_interval']
        y_interval = file_metadata['y_interval']
        z_interval = file_metadata['z_interval']
        split_percentage = file_metadata['split_percentage']
        number_of_clusters = file_metadata['number_of_clusters']

        # split data
        train_data_length = int(split_percentage / 100.0 * len(locations))
        train_data = locations[0: train_data_length]
        test_data = locations[train_data_length:]

        model = KMeans()
        random.seed(datetime.now())

        #FIRST TEST
        prepared_means = prepare_means(x_interval,y_interval,z_interval,number_of_clusters)
        
        model.fit(train_data, \
                number_of_clusters, \
                x_interval, \
                y_interval, \
                z_interval, \
                KMeans.HALT_STRATEGY_TYPES['convergence'], \
                halt_threshold=0.0000000000000001, \
                mean_start_strategy=KMeans.MEAN_START_STRATEGY_TYPES['random'], \
                verbose=True)
     
        labels = model.predict(test_data)

        print labels

        plotter.plot_clusters(model)
        
        rebel_leader_location = model.locate_rebel_leader()
        rebel_base_location = model.locate_rebel_base()

        print "REBEL LEADER IS LOCATED HERE:", rebel_leader_location
        print "REBEL BASE IS LOCATED HERE:", rebel_base_location
        #SECOND TEST
        prepared_means = prepare_means(x_interval,y_interval,z_interval,number_of_clusters)
        
        model.fit(train_data, \
                number_of_clusters, \
                x_interval, \
                y_interval, \
                z_interval, \
                KMeans.HALT_STRATEGY_TYPES['convergence'], \
                halt_threshold=0.0000000000000001, \
                mean_start_strategy=KMeans.MEAN_START_STRATEGY_TYPES['initialized'], \
                cluster_means=prepared_means, \
                verbose=True)
     
        labels = model.predict(test_data)

        print labels

        plotter.plot_clusters(model)
        rebel_leader_location = model.locate_rebel_leader()
        rebel_base_location = model.locate_rebel_base()

        print "REBEL LEADER IS LOCATED HERE:", rebel_leader_location
        print "REBEL BASE IS LOCATED HERE:", rebel_base_location
        #THIRD TEST
        prepared_means = prepare_means(x_interval,y_interval,z_interval,number_of_clusters)
        
        model.fit(train_data, \
                number_of_clusters, \
                x_interval, \
                y_interval, \
                z_interval, \
                KMeans.HALT_STRATEGY_TYPES['iterations'], \
                halt_iterations=1, \
                mean_start_strategy=KMeans.MEAN_START_STRATEGY_TYPES['initialized'], \
                cluster_means=prepared_means, \
                verbose=True)
     
        labels = model.predict(test_data)

        print labels

        plotter.plot_clusters(model)
        rebel_leader_location = model.locate_rebel_leader()
        rebel_base_location = model.locate_rebel_base()

        print "REBEL LEADER IS LOCATED HERE:", rebel_leader_location
        print "REBEL BASE IS LOCATED HERE:", rebel_base_location

        #FOURTH TEST
        prepared_means = prepare_means(x_interval,y_interval,z_interval,number_of_clusters)
        

        model.fit(train_data, \
                number_of_clusters, \
                x_interval, \
                y_interval, \
                z_interval, \
                KMeans.HALT_STRATEGY_TYPES['iterations'], \
                halt_iterations=10, \
                mean_start_strategy=KMeans.MEAN_START_STRATEGY_TYPES['initialized'], \
                cluster_means=prepared_means, \
                verbose=True)
     
        labels = model.predict(test_data)

        print labels

        plotter.plot_clusters(model)
        
        rebel_leader_location = model.locate_rebel_leader()
        rebel_base_location = model.locate_rebel_base()

        print "REBEL LEADER IS LOCATED HERE:", rebel_leader_location
        print "REBEL BASE IS LOCATED HERE:", rebel_base_location
        # You can implement your own tests just like I did below.
        # This file is not for evaluation.
        
        """
        plotter.plot_points(train_data)

        model.fit(train_data, \
                  number_of_clusters, \
                  x_interval, \
                  y_interval, \
                  z_interval, \
                  KMeans.HALT_STRATEGY_TYPES['convergence'], \
                  halt_threshold=0.0000000000000001, \
                  mean_start_strategy=KMeans.MEAN_START_STRATEGY_TYPES['random'], \
                  verbose=True)

        # below sample code is for preparing means from outside

        # prepared_means = prepare_means(x_interval,y_interval,z_interval,number_of_clusters)
        # 
        # model.fit(train_data, \
        #           number_of_clusters, \
        #           x_interval, \
        #           y_interval, \
        #           z_interval, \
        #           KMeans.HALT_STRATEGY_TYPES['iterations'], \
        #           halt_iterations=10, \
        #           mean_start_strategy=KMeans.MEAN_START_STRATEGY_TYPES['initialized'], \
        #           cluster_means=prepared_means, \
        #           verbose=True)
        
        labels = model.predict(test_data)

        print labels

        plotter.plot_clusters(model)

        rebel_leader_location = model.locate_rebel_leader()
        rebel_base_location = model.locate_rebel_base()

        print "REBEL LEADER IS LOCATED HERE:", rebel_leader_location
        print "REBEL BASE IS LOCATED HERE:", rebel_base_location

        plotter.plot_for_empire(model, rebel_leader_location, rebel_base_location, show_rebels=True, show_imperials=True)
        """


if __name__ == "__main__":
    main()
