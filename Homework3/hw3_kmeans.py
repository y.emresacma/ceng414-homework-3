import random
import math
import numpy as np

"""
    Find distance between two points in 3D space
"""


def distance(point1, point2):
    return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2 + (point1[2] - point2[2]) ** 2)


"""
    Generates means with given intervals.
    Mean count is number_of_clusters.
    THIS IS NOT USED FOR GENERATING RANDOM MEANS.
    DO NOT use this function in your fit() implementation. Use it only to write your own tests in hw3_main.py
"""


def prepare_means(x_interval, y_interval, z_interval, number_of_clusters):
    means = []
    step_ratio = 1.0 / (number_of_clusters + 1)

    x_range = x_interval[1] - x_interval[0]
    y_range = y_interval[1] - y_interval[0]
    z_range = z_interval[1] - z_interval[0]

    for i in range(number_of_clusters):
        means.append((x_interval[0] + (i + 1) * 0.5 * step_ratio * x_range,
                      y_interval[0] + (i + 1) * 0.5 * step_ratio * y_range,
                      z_interval[0] + (i + 1) * 0.5 * step_ratio * z_range))

    return means


class KMeans:
    #
    #   Cluster means can be generated randomly before training.
    #   Cluster means can be prepared outside and given to fit function
    #
    MEAN_START_STRATEGY_TYPES = {
        'random': 0,
        'initialized': 1,
    }

    #
    #   Model can halt after it converges to a point or can halt after a number of iterations.
    #
    HALT_STRATEGY_TYPES = {
        'convergence': 0,
        'iterations': 1,
    }

    """
        Explanations for Kmeans class members:
        
        data:                   Specifies list of points that are clustered. It's the training data.
        
        number_of_clusters:     Specifies cluster count. If this is not set, model does not fit to your data.
        
        cluster_means:          Specifies means of each cluster. If there are n number of clusters, there should be n cluster means.
                                'cluster_means' member should be filled inside fit() function.
                                
        cluster_elems:          Specifies lists of points in each cluster. If there are n clusters, there are n lists of points 
                                that are constructing a cluster. 'cluster_elems' member should be filled inside fit() function.
        
        cluster_colors:         Specifies distinct colors for clusters. This member is handled in 'hw3_kmeans_plotter.py' automatically.
                                You don't need to do any cluster coloring operation.
                                
                                
        *** Make sure you keep skeleton structure that is given to you. YOU CAN ADD TO THE SKELETON STRUCTURE, BUT CAN NOT REMOVE FROM IT. ***
    """

    def __init__(self):
        self.data = []
        self.number_of_clusters = 0
        self.cluster_means = []
        self.cluster_elems = []
        self.cluster_colors = []

    """
        This function generates a random point in 3D space.
        You should call this function from fit() function, when mean start strategy is "random".
    """

    def generate_random_point_in_space(self, x_interval, y_interval, z_interval, cluster_index):
        random.seed(cluster_index)
        x_coord = random.uniform(x_interval[0], x_interval[1])
        y_coord = random.uniform(y_interval[0], y_interval[1])
        z_coord = random.uniform(z_interval[0], z_interval[1])
        return x_coord, y_coord, z_coord

    """
        Calculate mean of the cluster with cluster_index as (mean_x, mean_y, mean_z)
        You can use this function later in your fit() function implementation
    """

    def calculate_cluster_mean(self, cluster_index):
        totalx = 0
        totaly = 0
        totalz = 0
        if len(self.cluster_elems[cluster_index]) == 0:
            return 0, 0, 0

        for i in range(len(self.cluster_elems[cluster_index])):
            totalx += self.cluster_elems[cluster_index][i][0]
            totaly += self.cluster_elems[cluster_index][i][1]
            totalz += self.cluster_elems[cluster_index][i][2]

        totalx /= len(self.cluster_elems[cluster_index])
        totaly /= len(self.cluster_elems[cluster_index])
        totalz /= len(self.cluster_elems[cluster_index])

        return totalx, totaly, totalz
    """
        Run k-means algorithm to group elements

        There are 2 types of halting strategies. Model can halt after it converges to a point or can halt after a number of iterations.
            1.    When halting strategy is convergence, only halting threshold is considered. Even if halt_iterations parameter is given, it is ignored.
            2.    When halting strategy is number of iterations, only number of iterations is considered. Even if halt_threshold parameter is given, it is ignored.
        
        There are 2 types to mean start strategies.
            1.   Cluster means can be generated randomly before training. Even if cluster_means parameter is given, it is ignored.
            2.   Cluster means can be prepared outside and given to fit function. If this is the aim, cluster_means parameter should be set
                and length of cluster_means should be equal to number_of_clusters. Otherwise, random strategy is applied.
        
        There are 4 types of training sytles in total. Parameter explanations are below:

        train_data:             Data to be processed                                                                                                                          REQUIRED
        number_of_clusters:     Specifies how many clusters should be created from train_data                                                                                 REQUIRED
        x_interval:             Tuple stating the minimum and maximum values for X coordinates of points in train_data                                                        REQUIRED
        y_interval:             Tuple stating the minimum and maximum values for Y coordinates of points in train_data                                                        REQUIRED
        z_interval:             Tuple stating the minimum and maximum values for Z coordinates of points in train_data                                                        REQUIRED
        halt_strategy:          Specifies how training stops, default value is 0 ('convergence'). See HALT_STRATEGY_TYPES above.                                              OPTIONAL
        halt_threshold:         Specifies mean difference threshold (default is 0.00001) to stop training. Should be ignored when halt_strategy is 1 ('iterations').          OPTIONAL
        halt_iterations:        Specifies how many iterations are done while training. Should be ignored when halt_strategy is 0 ('convergence').                             OPTIONAL
        mean_start_strategy:    Specifies how cluster means are initialized, default value is 0 ('random'). See MEAN_START_STRATEGY_TYPES above.                              OPTIONAL
        cluster_means:          This parameter should be assigned when means are given from outside. Should be ignored when mean_start_strategy is 0 ('random').              OPTIONAL
        verbose:                Prints outputs about training process. Default value is False (nothing is printed).                                                           OPTIONAL
                                See homework text for printing format.

        *** Read init() function comments for detailed information about class members. ***
    """
    def distance(self, point1, point2):
        return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2 + (point1[2] - point2[2]) ** 2)

    def fit(self, \
            train_data, \
            number_of_clusters, \
            x_interval, \
            y_interval, \
            z_interval, \
            halt_strategy=HALT_STRATEGY_TYPES['convergence'], \
            halt_threshold=0.00001, \
            halt_iterations=0, \
            mean_start_strategy=MEAN_START_STRATEGY_TYPES['random'], \
            cluster_means=None, \
            verbose=False):

        # If data is empty or number_of_clusters is not specified, do nothing
        if train_data == None or len(train_data) == 0:
            print "ERROR[fit]: Please give non-empty data."
            return

        elif number_of_clusters == 0:
            print "ERROR[fit]: Please give non-zero number of clusters."
            return

        else:
            self.data = train_data
            self.number_of_clusters = number_of_clusters
            self.cluster_means = []
            self.cluster_elems = []
            self.cluster_colors = []
        #Set the cluster means which depends on starting strategy
        if mean_start_strategy == 0:
            for i in range(self.number_of_clusters):
                self.cluster_means.append(self.generate_random_point_in_space(x_interval, y_interval, z_interval, i))
            
        elif mean_start_strategy == 1:
            for i in range(len(cluster_means)):
                self.cluster_means.append(cluster_means[i])
        
        initial_clusters_means = []
        for i in range(len(self.cluster_means)):
            initial_clusters_means.append(self.cluster_means[i])
        
        #initialize elements of cluster means
        for i in range(self.number_of_clusters):
            self.cluster_elems.append([])
        #Add elements to the clusters according to minimum distance
        for i in range(len(self.data)):
            minIndex = 0
            minDistance = self.distance(self.cluster_means[0], self.data[i])
            j = 1
            while (j < self.number_of_clusters):
                distance = self.distance(self.cluster_means[j], self.data[i])
                if distance < minDistance:
                    minDistance = distance
                    minIndex = j
                j = j + 1
            
            self.cluster_elems[minIndex].append(self.data[i])

        

        #if strategy is convergence, recompute clusters until mean change is not significant
        if (halt_strategy == 0):
            oldMeans = []
            newMeans = []
            for i in range(self.number_of_clusters):
                oldMeans.append(self.cluster_means[i])

            for i in range(self.number_of_clusters):
                mean = self.calculate_cluster_mean(i)
                newMeans.append(mean)     
            
            a = 0
            
            while True:
                flag = 0
                cluster_elements = []
                for i in range(self.number_of_clusters):
                    cluster_elements.append([])
                for i in range(self.number_of_clusters):
                    if self.distance(oldMeans[i], newMeans[i]) < halt_threshold:
                        flag += 1
                
                if flag == self.number_of_clusters:
                    break

                for i in range(len(self.cluster_means)):
                    oldMeans[i] = self.cluster_means[i]
                oldMeans = self.cluster_means
                for i in range(len(newMeans)):
                    self.cluster_means[i] = newMeans[i]



                for i in range(len(self.data)):
                    minIndex = 0
                    minDistance = self.distance(self.cluster_means[0], self.data[i])
                    j = 1
                    while (j < self.number_of_clusters):
                        distance = self.distance(self.cluster_means[j], self.data[i])
                        if distance < minDistance:
                            minDistance = distance
                            minIndex = j
                        j = j + 1
                    cluster_elements[minIndex].append(self.data[i])
                self.cluster_elems = cluster_elements
                for i in range(self.number_of_clusters):
                    mean = self.calculate_cluster_mean(i)
                    newMeans[i] = mean

                
        elif (halt_strategy == 1):
            
            iterations = 0
            #recompute clusters means
            
            while True:
                i = 0
                while i < len(self.cluster_means):
                    mean = self.calculate_cluster_mean(i)
                    self.cluster_means[i] = mean
                    i += 1       
                iterations += 1  
                if iterations == halt_iterations:
                    break
                #initialize new cluster elements
                cluster_elements = []
                for i in range(self.number_of_clusters):
                    cluster_elements.append([])
                #Rearrange cluster's elements according to new means
                for i in range(len(self.data)):
                    minIndex = 0
                    minDistance = self.distance(self.cluster_means[0], self.data[i])
                    j = 1
                    while (j < self.number_of_clusters):
                        distance = self.distance(self.cluster_means[j], self.data[i])
                        if distance < minDistance:
                            minDistance = distance
                            minIndex = j
                        j += 1
                    cluster_elements[minIndex].append(self.data[i])
                for k in range(len(cluster_elements)):
                    self.cluster_elems[k] = cluster_elements[k]

                
        
        if (verbose == True):
            print "----- Configuration -----"
            print "Number of clusters: " + str(self.number_of_clusters)
            print "X Range: " + str(x_interval[0]) +" to " + str(x_interval[1])
            print "Y Range: " + str(y_interval[0]) +" to " + str(y_interval[1])
            print "Z Range: " + str(z_interval[0]) +" to " + str(z_interval[1])
            
            if (mean_start_strategy == 0):
                print "Mean start strategy: 'random'"
                for i in range(self.number_of_clusters):
                    print "    Cluster " + str(i) + " with random mean: (" + str(format(initial_clusters_means[i][0], '.12f')) + ", " + str(format(initial_clusters_means[i][1], '.12f')) + ", " + str(format(initial_clusters_means[i][2], '.12f')) + ")"
            elif (mean_start_strategy == 1):
                print "Mean start strategy: 'initialized'   "
                for i in range(self.number_of_clusters):
                    print "    Cluster " + str(i) + " with initialized mean: (" + str(format(initial_clusters_means[i][0], '.12f')) + ", " + str(format(initial_clusters_means[i][1], '.12f')) + ", " + str(format(initial_clusters_means[i][2], '.12f')) + ")"
            
            if halt_strategy == 0:
                print "Halt strategy: 'convergence'"
                print "Halt threshold: " + str(halt_threshold)
            elif halt_strategy == 1:
                print "Halt strategy: 'iterations'"
                print "Halt iterations: " + str(halt_iterations)
            print "----- Final Results -----"
            for i in range(number_of_clusters):
                print "Cluster " + str(i) + " mean: (" + str(format(self.cluster_means[i][0], '.12f')) + ", " + str(format(self.cluster_means[i][1], '.12f')) + ", " + str(format(self.cluster_means[i][2], '.12f')) + ") Number of points: " + str(len(self.cluster_elems[i]))
            



    """
        Return list of predicted labels of test data instances by looking at clusters generated before.
        Labels start from 0. If you have 4 clusters for example, your labels should be 0,1,2,3.
    """

    def predict(self, test_data):

        if test_data == None or len(test_data) == 0:
            print "ERROR[predict]: Please give non-empty data."
            return None

        elif self.number_of_clusters == 0:
            print "ERROR[predict]: Please fit your model, generate clusters from data first."
            return None

        else:
            result = []
            for i in range(len(test_data)):
                minIndex = 0
                minDistance = self.distance(test_data[i], self.cluster_means[0])
                j=1
                while j < self.number_of_clusters:
                    distance = self.distance(test_data[i], self.cluster_means[j])
                    if distance < minDistance:
                        minDistance = distance
                        minIndex = j
                    j += 1
                result.append(minIndex)
            return result
                

    """
        Locate the REBEL BASE.
        Find distances between any 2 cluster means. 
        Select 2 means that are closer to each other than the others. 
        Then return weighted average of these means.
        
        Return format is triple: (base_x, base_y, base_z)
    """

    def locate_rebel_base(self):

        if self.number_of_clusters == 0:
            print "ERROR[locate_rebel_base]: Please fit your model, generate clusters from data first."
            return None

        else:
            minDistance = 9999.99
            index1 = 0
            index2 = 0
            for i in range(self.number_of_clusters):
                j = i+1
                while j < self.number_of_clusters:
                    distance = self.distance(self.cluster_means[i], self.cluster_means[j])
                    if distance < minDistance:
                        minDistance = distance
                        index1 = i
                        index2 = j
                    j += 1
            a = (self.cluster_means[index1][0] * len(self.cluster_elems[index1]) + self.cluster_means[index2][0] * len(self.cluster_elems[index2])) / (len(self.cluster_elems[index1]) + len(self.cluster_elems[index2]))
            b = (self.cluster_means[index1][1] * len(self.cluster_elems[index1]) + self.cluster_means[index2][1] * len(self.cluster_elems[index2])) / (len(self.cluster_elems[index1]) + len(self.cluster_elems[index2]))
            c = (self.cluster_means[index1][2] * len(self.cluster_elems[index1]) + self.cluster_means[index2][2] * len(self.cluster_elems[index2])) / (len(self.cluster_elems[index1]) + len(self.cluster_elems[index2]))
            return a, b, c
    """
        Locate the REBEL LEADER, LEIA ORGANA, among the rebel activities.
        Imperial intelligence tells that the escaping rebel leader is the closest one to the cluster mean with maximum number of rebel activity.
        Return the location of the rebel leader.
        Return format: (leader_x, leader_y, leader_z) 
    """

    def locate_rebel_leader(self):

        if self.number_of_clusters == 0:
            print "ERROR[locate_rebel_leader]: Please fit your model, generate clusters from data first."
            return None

        else:
            maxNumber = len(self.cluster_elems[0])
            index = 0
            i = 1
            while i < self.number_of_clusters:
                number = len(self.cluster_elems[i])
                if number > maxNumber:
                    index = i
                    maxNumber = number
                i += 1
         
            minDistance = self.distance(self.cluster_means[index], self.cluster_elems[index][0])
            elemIndex = 0
            i = 1
            while i < len(self.cluster_elems[index]):
                distance = self.distance(self.cluster_elems[index][i], self.cluster_means[index])
                if distance < minDistance:
                    minDistance = distance
                    elemIndex = i
                i += 1

            a = self.cluster_elems[index][elemIndex][0]
            b = self.cluster_elems[index][elemIndex][1]
            c = self.cluster_elems[index][elemIndex][2]
            
            return a, b, c