"""
    Read input file to get locations of points. Save and return necessary data using following format:

        file_metadata['locations'] = list of locations

        file_metadata['x_interval'] = (x_min, x_max)

        file_metadata['y_interval'] = (y_min, y_max)

        file_metadata['z_interval'] = (z_min, z_max)

        file_metadata['split_percentage'] = split percentage

        file_metadata['number_of_clusters'] = number of clusters
"""
def read_input_file(input_file_name):
    file_metadata = {}
    file = open(input_file_name, 'r')
    myList = []
    # Read data from file ine by line, store it in myList
    for line in file:
        myList.append(line)
    #Split the data
    xRange = [float(x.strip()) for x in myList[0].split(',')]
    yRange = [float(x.strip()) for x in myList[1].split(',')]
    zRange = [float(x.strip()) for x in myList[2].split(',')]
    split_percentage = int(myList[3])
    number_of_clusters = int(myList[4])
    number_of_points = int(myList[5])
    points = []
    i = 0
    while i < number_of_points :
        points.append([float(x.strip()) for x in myList[i + 6].split(',')])
        i = i + 1
    #Store in dictionary
    file_metadata['locations'] = points
    file_metadata['x_interval'] = xRange
    file_metadata['y_interval'] = yRange
    file_metadata['z_interval'] = zRange
    file_metadata['split_percentage'] = split_percentage
    file_metadata['number_of_clusters'] = number_of_clusters

    return file_metadata
read_input_file('inputs/points_75_split80_noc5_yz.txt')