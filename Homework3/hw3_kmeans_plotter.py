import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.lines as mlines

color_scala = [(1.0, 0.2, 0.2), (1.0, 0.6, 0.2), (1.0, 1.0, 0.2), (0.2, 1.0, 0.2), (0.2, 1.0, 1.0), (0.2, 0.6, 1.0),
               (0.2, 0.2, 1.0), (0.6, 0.2, 1.0), (1.0, 0.2, 1.0), (0.6, 0.6, 0.6), (0.8, 0.0, 0.0), (0.8, 0.4, 0.0),
               (0.8, 0.8, 0.0), (0.0, 0.8, 0.0), (0.0, 0.8, 0.8), (0.0, 0.4, 0.8), (0.0, 0.0, 0.8), (0.4, 0.0, 0.8),
               (0.8, 0.0, 0.8), (0.3, 0.3, 0.3)]

"""
    Plots points in clusters with different color, in 3D space.
    Each cluster has different, randomly generated color.
    Randomly generated colors can be indistinguishable sometimes, no big deal :)
    Please examine explanations below, to understand matplotlib's plotting function parameters.
    You can play with this plotter. You may use them in your other projects in future.

    MARKER TYPES
    You can find possible markers for plotting below.
    For more information, visit https://matplotlib.org/api/markers_api.html

    ============================== ===============================================
    marker                         description
    ============================== ===============================================
    `"."`                          point
    `","`                          pixel
    `"o"`                          circle
    `"v"`                          triangle_down
    `"^"`                          triangle_up
    `"<"`                          triangle_left
    `">"`                          triangle_right
    `"1"`                          tri_down
    `"2"`                          tri_up
    `"3"`                          tri_left
    `"4"`                          tri_right
    `"8"`                          octagon
    `"s"`                          square
    `"p"`                          pentagon
    `"P"`                          plus (filled)
    `"*"`                          star
    `"h"`                          hexagon1
    `"H"`                          hexagon2
    `"+"`                          plus
    `"x"`                          x
    `"X"`                          x (filled)
    `"D"`                          diamond
     "d"`                          thin_diamond
    `"|"`                          vline
    `"_"`                          hline

    COLOR TYPES
    While plotting, c parameter is for setting color of the points.
    Below colors are basic colors:
    Blue('b'), Green('g'), Red('r'), Cyan('c'), Magenta('m'), Yellow('y'), Black('k'), White('w')

    To set color of points as custom points, set c parameter as (red value, green value, blue value)
    where red, green, blue values are floats in 0-1 range (0 for 0, 1 for 255).
    For more information, visit https://matplotlib.org/api/colors_api.html

"""


def plot_clusters(kmeans_model):
    if kmeans_model.number_of_clusters == 0:
        print "ERROR[plot_clusters]: Please fit your model, generate clusters from data first."
        return

    if len(kmeans_model.cluster_colors) == 0:
        kmeans_model.cluster_colors = [get_color(i) for i in range(kmeans_model.number_of_clusters)]

    fig = plt.figure(figsize=(14, 8))
    fig.canvas.set_window_title('02 K-Means Clustering')
    fig.suptitle('Points in ' + str(kmeans_model.number_of_clusters) + ' clusters', fontsize=16)

    ax = fig.add_subplot(111, projection='3d')

    for i in range(kmeans_model.number_of_clusters):
        cluster_x_values = []
        cluster_y_values = []
        cluster_z_values = []

        for point in kmeans_model.cluster_elems[i]:
            cluster_x_values.append(point[0])
            cluster_y_values.append(point[1])
            cluster_z_values.append(point[2])

        # draw cluster means as diamonds
        ax.scatter([kmeans_model.cluster_means[i][0]], [kmeans_model.cluster_means[i][1]],
                   [kmeans_model.cluster_means[i][2]], c=kmeans_model.cluster_colors[i], marker='D', s=64,
                   edgecolor='k', label='Cluster ' + str(i + 1) + ' mean')

        # draw cluster points as circles
        ax.scatter(cluster_x_values, cluster_y_values, cluster_z_values, c=kmeans_model.cluster_colors[i], marker='o',
                   edgecolor='k', label='Cluster ' + str(i + 1) + " points")

    ax.set_xlabel('X coordinates')
    ax.set_ylabel('Y coordinates')
    ax.set_zlabel('Z coordinates')

    plt.legend(loc='upper left', fontsize='x-small')

    plt.show()


"""
    Use this function to show rebel leader, rebel base, vader, death star, imperial star destroyer locations.
    Make sure you have found rebel leader and rebel base locations.
    Location of Lord Vader is close to the rebel leader, location of the Death Star is close to the rebel base.
    Location of star destroyers are close to cluster means.

    To display rebel leader, rebel base and imperial forces, you have to locate rebel leader and rebel base first.
    When both locations are located,
        If show_rebels is True:
            - It plots rebel leader Leia Organa with a white star
            - It plots rebel base with a white triangle.

        If show_imperials is True:
            - It plots star destroyers with red right triangle,
            - It plots the Death Star as red circle
            - It plots Lord Vader as red large X
"""


def plot_for_empire(kmeans_model, rebel_leader_location=None, rebel_base_location=None, show_rebels=False,
                    show_imperials=False):
    if kmeans_model.number_of_clusters == 0:
        print "ERROR[plot_for_empire]: Please fit your model, generate clusters from data first."
        return

    if rebel_base_location == None:
        print "WARNING[plot_for_empire]: Rebel base IS NOT LOCATED yet."

    if rebel_leader_location == None:
        print "WARNING[plot_for_empire]: Rebel leader Leia Organa IS NOT LOCATED yet."

    if len(kmeans_model.cluster_colors) == 0:
        kmeans_model.cluster_colors = [get_color(i) for i in range(kmeans_model.number_of_clusters)]

    fig = plt.figure(figsize=(14, 8))
    fig.canvas.set_window_title('03 Battle of Homework 3')
    fig.suptitle('Rebel activities in ' + str(kmeans_model.number_of_clusters) + ' clusters', fontsize=16)
    ax = fig.add_subplot(111, projection='3d')

    for i in range(kmeans_model.number_of_clusters):

        cluster_x_values = []
        cluster_y_values = []
        cluster_z_values = []

        if show_rebels and rebel_leader_location != None:

            for point in kmeans_model.cluster_elems[i]:

                if point[0] != rebel_leader_location[0] and point[0] != rebel_leader_location[1] and point[0] != \
                        rebel_leader_location[2]:
                    cluster_x_values.append(point[0])
                    cluster_y_values.append(point[1])
                    cluster_z_values.append(point[2])
        else:
            for point in kmeans_model.cluster_elems[i]:
                cluster_x_values.append(point[0])
                cluster_y_values.append(point[1])
                cluster_z_values.append(point[2])

        # draw cluster means as diamonds
        ax.scatter([kmeans_model.cluster_means[i][0]], [kmeans_model.cluster_means[i][1]],
                   [kmeans_model.cluster_means[i][2]], c=kmeans_model.cluster_colors[i], marker='D', s=64,
                   label='Cluster mean ' + str(i + 1))

        # draw cluster points as circles
        ax.scatter(cluster_x_values, cluster_y_values, cluster_z_values, c=kmeans_model.cluster_colors[i], marker='o',
                   label='Rebel activity in cluster ' + str(i + 1))

    # draw star destroyers as triangle right, close to rebel activity cluster means
    if show_imperials and rebel_leader_location != None and rebel_base_location != None:
        star_destroyer_x = []
        star_destroyer_y = []
        star_destroyer_z = []

        for i in range(kmeans_model.number_of_clusters):
            star_destroyer_x.append(kmeans_model.cluster_means[i][0] - 200)
            star_destroyer_y.append(kmeans_model.cluster_means[i][1] - 200)
            star_destroyer_z.append(kmeans_model.cluster_means[i][2] - 200)

        ax.scatter(star_destroyer_x, star_destroyer_y, star_destroyer_z, c='r', marker='>', s=81, edgecolor='w',
                   label='Imperial Star Destroyers')

    # draw rebel base as triangle up
    if show_rebels and rebel_leader_location != None and rebel_base_location != None:
        ax.scatter([rebel_base_location[0]], [rebel_base_location[1]], [rebel_base_location[2]], c='w', marker='^',
                   s=144, edgecolor=(0.4, 0.4, 0.4), label='Rebel Base')

    # draw death star as circle, close to the rebel base
    if show_imperials and rebel_leader_location != None and rebel_base_location != None:
        ax.scatter([rebel_base_location[0] - 200], [rebel_base_location[1] - 200], [rebel_base_location[2] - 200],
                   c='r', marker='o',
                   s=216, edgecolor='w', label='Death Star')

    # draw rebel leader as star
    if show_rebels and rebel_leader_location != None and rebel_base_location != None:
        ax.scatter([rebel_leader_location[0]], [rebel_leader_location[1]], [rebel_leader_location[2]], c='w',
                   marker='*', s=144, edgecolor=(0.4, 0.4, 0.4), label='Rebel Leader')

    # draw vader as large X
    if show_imperials and rebel_leader_location != None and rebel_base_location != None:
        ax.scatter([rebel_leader_location[0] - 200], [rebel_leader_location[1] - 200],
                   [rebel_leader_location[2] - 200], c='r', marker='X', s=144, edgecolor='w', label='Lord Vader')

    plt.legend(loc='upper left', fontsize='small')

    ax.set_xlabel('X coordinates')
    ax.set_ylabel('Y coordinates')
    ax.set_zlabel('Z coordinates')
    ax.w_xaxis.set_pane_color((0.0, 0.0, 0.0, 0.9))
    ax.w_yaxis.set_pane_color((0.0, 0.0, 0.0, 0.9))
    ax.w_zaxis.set_pane_color((0.0, 0.0, 0.0, 0.9))

    plt.show()


"""
    Get color triple, compatible with matplotlib's plotter.
    If amount of distinct colors is not enough, generates random color.
"""


def get_color(color_index):
    if color_index < len(color_scala):
        return color_scala[color_index]
    else:
        return (random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1))


"""
    View raw data points on 3D space.
"""


def plot_points(point_list):
    if point_list == None or len(point_list) == 0:
        return

    fig = plt.figure(figsize=(14, 8))
    fig.canvas.set_window_title('01 Data Visualization')
    fig.suptitle('Points on 3D space', fontsize=16)

    ax = fig.add_subplot(111, projection='3d')

    point_x_values = []
    point_y_values = []
    point_z_values = []

    for point in point_list:
        point_x_values.append(point[0])
        point_y_values.append(point[1])
        point_z_values.append(point[2])

    # draw cluster points as circles
    ax.scatter(point_x_values, point_y_values, point_z_values, c=(0.0, 0.0, 1.0), marker='o', edgecolor='k')

    ax.set_xlabel('X coordinates')
    ax.set_ylabel('Y coordinates')
    ax.set_zlabel('Z coordinates')

    plt.show()
